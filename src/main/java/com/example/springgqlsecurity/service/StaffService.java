package com.example.springgqlsecurity.service;

import com.example.springgqlsecurity.model.Staff;
import com.example.springgqlsecurity.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class StaffService {

    @Autowired
    StaffRepository staffRepository;


    public Staff createStaff(String name, String gender, Double salary) {
        var staff = staffRepository.save(Staff.builder().name(name).gender(gender).salary(salary).build());
        return staff;
    }

    public Staff updateStaff(Integer id, String name) {
        var staff = staffRepository.getReferenceById(id);
        if (Objects.nonNull(name)) {
            staff.setName(name);
            return staffRepository.save(staff);
        }

        return staff;
    }

    public void deleteStaff(Integer id) {
        var staff = staffRepository.getReferenceById(id);
        staffRepository.deleteById(id);
    }



    public Optional<Staff> getStaffById(Integer id) {
        return staffRepository.findById(id);
    }

    public Page<Staff> getStaffs(String name, String gender, Pageable pageable) {

        if (Objects.isNull(pageable)) {
            pageable = PageRequest.of(0, 10);
        }

        var staffBuilder = Staff.builder();
        if (Objects.nonNull(name)) {
            staffBuilder.name(name);
        } else if (Objects.nonNull(gender)) {
            staffBuilder.gender(gender);
        }
        var example = Example.of(staffBuilder.build());
        final var newPageable = pageable;
        var staffs = staffRepository.findBy(example, staffFetchableFluentQuery -> staffFetchableFluentQuery.page(newPageable).stream().toList());
        var count  = staffRepository.findBy(example, staffFetchableFluentQuery -> staffFetchableFluentQuery.stream().count());

        return new PageImpl<>(staffs, pageable, count);
    }

}
