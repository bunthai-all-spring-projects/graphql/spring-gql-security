package com.example.springgqlsecurity.gqlcontroller;

import com.example.springgqlsecurity.model.Staff;
import com.example.springgqlsecurity.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
public class StaffGraphQLController {

    @Autowired
    StaffService staffService;

    @QueryMapping
    public Page<Staff> getStaffs() {
        return staffService.getStaffs(null, null, null);
    }

    @QueryMapping
    public Staff getStaffById(@Argument("id") Integer id) {
        return staffService.getStaffById(id).orElse(null);
    }

}
