package com.example.springgqlsecurity;

import com.example.springgqlsecurity.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGqlSecurityApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringGqlSecurityApplication.class, args);
	}

	@Autowired
	StaffService staffService;

	@Override
	public void run(String... args) throws Exception {
		staffService.createStaff("Jack", "Male", 2000d);
		staffService.createStaff("Marry", "Female", 1000d);
		staffService.createStaff("Jose", "Male", 500d);
	}
}
